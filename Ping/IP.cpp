#include "IP.h"

unsigned short IpPacket::CalculateCheckSum(void* data, unsigned short size)
{
	unsigned short* buffer = (unsigned short*)data;
	unsigned long cksum = 0;

	while (size > 1)
	{
		cksum += *buffer++;
		size -= sizeof(unsigned short);
	}

	if (size)
	{
		cksum += *(unsigned char*)buffer;
	}

	cksum = (cksum >> 16) + (cksum & 0xffff);
	cksum += (cksum >> 16);

	return (unsigned short)(~cksum);
}

IpPacket::IpPacket(in_addr iaSrc, in_addr iaDsr, unsigned short sizeParam, u_char TTL)
	: size(sizeParam + sizeof(IpHeader))
{
	ipHeader = (IpHeader*)calloc(size, 1);
	ipHeader->h_len = sizeof(IpHeader) >> 2;
	ipHeader->version = 4;
	ipHeader->TOS = 0;
	ipHeader->TTL = TTL;
	ipHeader->TotLen = htons(size);
	ipHeader->FlagOff = 0;
	ipHeader->ID = htons(4321);
	ipHeader->Checksum = 0;
	ipHeader->iaSrc = iaSrc;
	ipHeader->iaDst = iaDsr;
}

IpPacket::IpPacket(char* data, unsigned short sizeParam) : size(sizeParam)
{
	ipHeader = (IpHeader*)malloc(size);
	memcpy_s((void*)ipHeader, size, (const void*)data, sizeParam);
}

IpHeader* IpPacket::header()
{
	return ipHeader;
}

void* IpPacket::data()
{
	return (void*)((char*)ipHeader + ipHeader->h_len * 4);
}

unsigned short IpPacket::dataLength() const
{
	return size - ipHeader->h_len * 4;
}

int IpPacket::send(SOCKET sd, sockaddr_in* da)
{
	ipHeader->Checksum = CalculateCheckSum((void*)ipHeader, size);
	return sendto(sd, (const char*)ipHeader, (int)size, 0, (const sockaddr*)da, sizeof(sockaddr_in));
}

int IpPacket::recv(SOCKET sd)
{
	int fromlen = sizeof(sockaddr_in);
	sockaddr_in from_addr;

	if (ipHeader)
		free(ipHeader);
	ipHeader = (IpHeader*)malloc(size = 0xFFFF);

	int received = recvfrom(sd, (char*)ipHeader, size, 0, (sockaddr*)&from_addr, &fromlen);
	if (received != SOCKET_ERROR)
		ipHeader = (IpHeader*)realloc((void*)ipHeader, size = received);

	return received;
}

IpPacket::~IpPacket()
{
	if (ipHeader)
		free(ipHeader);
}