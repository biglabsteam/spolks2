#include "ICMP.h"

IcmpPacket::IcmpPacket(in_addr iaSrc, in_addr iaDsr, unsigned short sizeParam, u_char TTL)
	: ipPacket(iaSrc, iaDsr, sizeParam, TTL)
{
	header()->icmp_code = 0;
	header()->icmp_type = ICMP_ECHO;
	header()->icmp_seq = 0;
	header()->icmp_id = 0;
	header()->icmp_cksum = 0;
	ipPacket.header()->Protocol = IPPROTO_ICMP;
}

IcmpHeader* IcmpPacket::header()
{
	return (IcmpHeader*)ipPacket.data();
}

IpHeader* IcmpPacket::ipheader()
{
	return ipPacket.header();
}

int IcmpPacket::send(SOCKET sd, sockaddr_in* sa)
{
	header()->icmp_cksum = ipPacket.CalculateCheckSum((void*)header(), ipPacket.dataLength());
	return ipPacket.send(sd, sa);
}

int IcmpPacket::recv(SOCKET sd)
{
	return ipPacket.recv(sd);
}