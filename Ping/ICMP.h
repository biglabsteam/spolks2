#pragma once

#include "IP.h"

typedef struct {
	BYTE icmp_type;
	BYTE icmp_code;
	USHORT icmp_cksum;
	USHORT icmp_id;
	USHORT icmp_seq;
} IcmpHeader;

class IcmpPacket
{
	IpPacket ipPacket;
	IcmpPacket(const IcmpPacket&);
public:
	IcmpPacket(in_addr iaSrc, in_addr iaDsr, unsigned short sizeParam, u_char TTL = 128);
	IcmpHeader* header();
	IpHeader* ipheader();
	int send(SOCKET sd, sockaddr_in* sa);
	int recv(SOCKET sd);
};