#include "IP.h"
#include "ICMP.h"

int findString(char* strings[], char* str)
{
	int index = 0;
	while (strings[index])
	{
		if (!strcmp(strings[index], str))
			return index;
		index++;
	}
	return -1;
}

int getSelfAddress(sockaddr_in* addr, int number)
{
	char chInfo[64];
	int nAdapter = 0;
	if (!gethostname(chInfo, sizeof(chInfo)))
	{
		hostent *sh;
		sh = gethostbyname((char*)&chInfo);
		if (sh != NULL)
		{
			while (sh->h_addr_list[nAdapter])
			{
				if (nAdapter == number)
				{
					memcpy(&addr->sin_addr, sh->h_addr_list[nAdapter], sh->h_length);
					break;
				}
				nAdapter++;
			}
		}
	}
	return nAdapter;
}

int getSelfAddressCount()
{
	char chInfo[64];
	int nAdapter = 0;
	if (!gethostname(chInfo, sizeof(chInfo)))
	{
		hostent *sh;
		sh = gethostbyname((char*)&chInfo);
		if (sh != NULL)
			while (sh->h_addr_list[nAdapter])
				nAdapter++;
	}
	return nAdapter;
}

int printAddresses()
{
	int count = getSelfAddressCount();
	sockaddr_in tempaddr;
	for (int i = 0; i < count; i++)
	{
		getSelfAddress(&tempaddr, i);
		printf("%d %s\n", i, inet_ntoa(tempaddr.sin_addr));
	}
	return count;
}

int main(int argc, char* argv[])
{
	WSADATA data;
	WSAStartup(0x0202, &data);

	int numberOfAddress = 0;
	sockaddr_in selfAddress;
	memset(&selfAddress, 0, sizeof(selfAddress));
	selfAddress.sin_family = AF_INET;
	if (getSelfAddressCount() > 1)
	{
		printAddresses();
		scanf("%d", &numberOfAddress);
	}
	else
		numberOfAddress = 0;
	getSelfAddress(&selfAddress, numberOfAddress);

	char dstString[100];
	sockaddr_in dstAddress;
	memset(&dstAddress, 0, sizeof(dstAddress));
	dstAddress.sin_family = AF_INET;

	HOSTENT* host = 0;

	char *addrStrPtr = dstString;
	scanf("%s", dstString);

	host = gethostbyname(addrStrPtr);

	if ((dstAddress.sin_addr.S_un.S_addr = inet_addr(addrStrPtr)) == -1)
		dstAddress.sin_addr.S_un.S_addr = *(unsigned long*)host->h_addr_list[0];

	printf("Ping %s ip = %s\n", host->h_name, inet_ntoa(dstAddress.sin_addr));

	int count = DEFAULT_COUNT;
	if (findString(argv, "-n") != -1)
		count = atoi(argv[findString(argv, "-n") + 1]);

	int size = DEFAULT_SIZE;
	if (findString(argv, "-l") != -1)
		size = atoi(argv[findString(argv, "-l") + 1]);
	size -= sizeof(IpHeader) + sizeof(IcmpHeader);

	auto sd = socket(PF_INET, SOCK_RAW, IPPROTO_ICMP);
	if (sd == INVALID_SOCKET)
	{
		printf("WSASocket() failed: %d\n", WSAGetLastError());
		return -1;
	}

	int on = 1, off = 0;
	size = BUF_SIZE;
	setsockopt(sd, SOL_SOCKET, SO_RCVBUF, (const char*)&size, sizeof(size));
	setsockopt(sd, IPPROTO_IP, IP_HDRINCL, (char *)&on, sizeof(on));
	int timeout = 10000;
	setsockopt(sd, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout));

	unsigned start_time;
	for (int i = 0; i < count; i++)
	{
		IcmpPacket icmpPacket((in_addr)selfAddress.sin_addr, (in_addr)dstAddress.sin_addr, size);

		printf("Ping from %s ", inet_ntoa(icmpPacket.ipheader()->iaSrc));

		icmpPacket.header()->icmp_id = i;
		start_time = GetTickCount();
		auto result = icmpPacket.send(sd, &dstAddress);
		result = icmpPacket.recv(sd);
		if (result != -1)
		{
			if (icmpPacket.header()->icmp_id == i)
				printf("to %s time %d ms TTL = %d\n", inet_ntoa(icmpPacket.ipheader()->iaDst), GetTickCount() - start_time, icmpPacket.ipheader()->TTL);
		}
		else
			printf("timeout\n");
	}

	closesocket(sd);

	WSACleanup();
	return 0;
}