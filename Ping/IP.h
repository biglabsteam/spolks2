#pragma once

#include "Header.h"

typedef struct {
	unsigned char  h_len : 4;
	unsigned char  version : 4;
	u_char	TOS;
	short	TotLen;
	short	ID;
	short	FlagOff;
	u_char	TTL;
	u_char	Protocol;
	u_short	Checksum;
	struct	in_addr iaSrc;
	struct	in_addr iaDst;
} IpHeader;

class IpPacket
{
	IpHeader* ipHeader;
	unsigned short size;
	IpPacket(const IpPacket&);
public:
	static unsigned short CalculateCheckSum(void* data, unsigned short size);
	IpPacket(in_addr iaSrc, in_addr iaDsr, unsigned short sizeParam = 1000, u_char TTL = 128);
	IpPacket(char* data, unsigned short sizeParam);
	IpHeader* header();
	void* data();
	unsigned short dataLength() const;
	int send(SOCKET sd, sockaddr_in* sa);
	int recv(SOCKET sd);
	virtual ~IpPacket();
};