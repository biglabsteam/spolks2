#pragma once

#include <sys/types.h>
#include <winsock2.h>
#include <conio.h>
#include <stdio.h>
#include <time.h>
#include <WS2tcpip.h>
#include <process.h>

#define DEFAULT_SIZE 64
#define DEFAULT_COUNT 5
#define ICMP_ECHO 8
#define BUF_SIZE 60 * 1024